# MySQLi Layer
This is a part from the custom framework that was built few months ago. PHP 7.0 version already supported.

>I was playing around with sqlGenerator - creating mapper of all tables in the database. The reason was that I was not alone working on the database and each developer could just run sqlGenerator after every change made instead of writing separated mysql files for updates.

MySQLi layer is supposed to use the mapper file on CRUD operations, to prevent errors in case that table field would be missing.

Idea for sqlGenerator had another point of view: we were updating packages on different domains. From this I developed installation scripts and on each update, database should be updated too. Having up-to-data mapper file is supposed to easy the update process.

IN that installation script, I've read the mapper file and compare it with current database state, update it, if there something was missing / redundant.
