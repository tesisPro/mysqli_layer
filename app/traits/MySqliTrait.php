<?php //File: library/traits/MySqliTrait.php
/**
 * Summary: trait for database functionality
 */
/**
 * MySqliTrait
 *
 * PHP version 5.6 / 7.0
 *
 * @subpackage Custom_framework
 * @author     Tereza Simcic <tereza.simcic@gmail.com>
 * @copyright  2016 Tereza Simcic
 * @license    Tereza Simcic
 *
 *
 * @link       http://tesispro.com
 * @name       MySqliTrait.php
 *
 */

namespace Tesis\Traits;

trait MySqliTrait
{
  /**
   * getConnection description
   * @param  obj $conn
   * @return obj
   */
  public function getConnection($conn)
  {
    if(is_null($conn)){
      $this->db = new MysqliLayer();
    }
    else{
      $this->db = $conn;
    }
  }
  /**
   * existingConnections check existing connections (only testing)
   *
   * @return array
   */
  public function existingConnections()
  {
    $sql = "SHOW STATUS WHERE `variable_name` = 'Threads_connected';";
    $result = $this->db->query($sql);
    return sizeof($result);
  }

/*-------------------- end of trait ---------------*/
}
