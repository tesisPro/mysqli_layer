<?php //File: library/BaseModel.php
/* vim: set expandtab tabstop=2 shiftwidth=2 softtabstop=2: */
/**
 * Summary: BaseModel class is managing main queries for the application
 *          just few examples provided
 */
/**
 * BaseModel class- class for databases
 *                   (dealing with select/insert/update/delete sql statements)
 *
 * PHP version 5.6 / 7.0
 *
 * @package    myProject
 * @subpackage Custom_framework
 * @author     Tereza Simcic <tereza.simcic@gmail.com>
 * @copyright  2015 Tereza Simcic
 * @license    Tereza Simcic
 *
 *
 * @link       http://tesispro.com
 * @name       BaseModel.php
 *
 */

namespace Tesis;

use Tesis\Traits\MySqliTrait;


class BaseModel
{
  use MySqliTrait;

  /**
  * @access public
  * @var object
  */
  public $db;
  /**
  * @access public
  * @var string
  */
  public $date;
  /**
  * @access public
  * @var int
  */
  public $time;
  /**
  * @access public
  * @var string
  */
  public $remoteAddr;
  /**
  * @access public
  * @var string
  */
  public $table;
  /**
  * @access public
  * primary key of the table
  * @var string
  */
  public $tablePK;
  /**
  * @access public
  * if table has autoincrement attr enabled
  * @var bool
  */
  public $tableAutoincrement;
  /**
  * @access public
  * @var array
  */
  public $tableFields = [];
  /**
  * @access public
  * generated array of database tables with fields, PK key and AI attribute
  * @var array
  */
  public $tables = [];
  /**
  * @access public
  * @var string
  */
  public $error;
  /**
   * __construct
   *
   * @param object $conn
   *
   * @return
   */
  public function __construct(MysqliLayer $conn=null, $tables='')
  {
    $this->getConnection($conn);

    $this->date = date('Y-m-d H:i:s');
    $this->time = time();
    $this->remoteAddr = REMOTE_ADDR;
    $this->tables = $tables;

  }


  /**
   * deleteUser(s) delete users and records from customers_users
   *
   * @param $userId id of the user
   *
   * @return bool
   */
  public function deleteUser($userId='', $single=true)
  {
    if(empty($userId)){
      return false;
    }
    if($single == true){
      $where = " WHERE id = " . $userId;
      $whereC = " WHERE userId = " . $userId;
    }
    
    else{
      $where = " WHERE id IN ( ". $userId ." )" ;
      $whereC = " WHERE userId IN ( ". $userId ." )" ;
    }
    $output = [];

    $deleteUsers = "DELETE FROM users " . $where;
    $results = $this->db->executeBySql($deleteUsers);

    
    return $results ? $results : false;
  }

  /**
   * updateUser
   *
   * @param  array $data array of data to update, id is excluded from array
   * @param  int   $userId
   *
   * @return bool
   */
  public function updateUser(array $data=null, $userId='')
  {
    if(is_null($data) || empty($userId)){
      return false;
    }
    $tableName = 'users';

    
    if(!isset($data['userId']) || empty($data['userId'])){
      $data['userId'] = $userId;
    }

    return $this->db->updateTable($tableName, $data);
  }

  /**
   * insertUIsettings insert settings: first insert will have defined fields,
   *                  will be inserted only once
   *
   * @param  int $locationNo location number
   *
   * @return bool/array
   */
  public function insertSettings(array $data=null, $id = '')
  {
    if(is_null($data) || empty($id)){
      $this->db->error = 'empty data or location: ' . $id;
      return false;
    }

    $tableName = 'settings';

    $data['created'] = $this->date;
    $data['savedIP'] = $this->remoteAddr;

    if(!isset($data['id']) || empty($data['id'])){
      $data['id'] = $id;
    }

    return $this->db->insertIntoTable($tableName, $data);
  }

  /**
   * getSiteSettings if site settings are not already saved, get them
   *                 (result is already shifted)
   *
   * @return object/bool
   */
  public function getSiteSettings()
  {
    return $this->db->selectRow('settings');
  }
  /**
   * updateSiteSettings
   *
   * @param  array $data       array of data to update, id is excluded from array
   * @param  int   $id id should always be 1
   *
   * @return bool
   */
  public function updateSiteSettings(array $data=null)
  {
    if(is_null($data)){
      return false;
    }

    $tableName = 'settings';

    return $this->db->updateTable($tableName, $data);
  }

  /*-------------- end class 1794----------------*/
}
