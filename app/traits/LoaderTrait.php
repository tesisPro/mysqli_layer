<?php //File: library/traits/LoaderTrait.php
/**
 * Summary: trait for reading config ini file, setting up constants and
 *          variables needed for optimal functionality
 */
/**
 * LoaderTrait
 *
 * PHP version 5.6 / 7.0
 *
 * @subpackage Custom_framework
 * @author     Tereza Simcic <tereza.simcic@gmail.com>
 * @copyright  2016 Tereza Simcic
 * @license    Tereza Simcic
 *
 *
 * @link       http://tesispro.com
 * @name       LoaderTrait.php
 *
 */

namespace Tesis\Traits;

trait LoaderTrait {

  public function loader($configFile='')
  {
    if(empty($configFile)){
        $configFile = 'env.ini';
    }

    $configIni = new Loader();
    $configGet = $configIni->loadIni($configFile);


    // Environment settings
    $this->environment    = $configGet->get('Environment');
    $envPointer = '';
    if( !null == $configGet->get('EnvPointer') && !empty($configGet->get('EnvPointer')) ){
      $envPointer = $configGet->get('EnvPointer');
    }
    $this->envPointer     = $envPointer;
    //important only for dev / release
    $baseUrl = 'tesispro.com';
    if(!null == $configGet->get('baseUrl') && !empty($configGet->get('baseUrl'))){
      $baseUrl = $configGet->get('baseUrl');
    }
    $this->baseUrl        = $baseUrl;
    $this->siteName       = $configGet->get('siteName');

    $cacheFolder = '';
    if(!null == $configGet->get('cacheFolder') && !empty($configGet->get('cacheFolder'))){
      $cacheFolder = $configGet->get('cacheFolder');
    }
    $this->cacheFolder    = $configGet->get('cacheFolder');

    $imageFolder = '';
    if(!null == $configGet->get('imageFolder') && !empty($configGet->get('imageFolder'))){
      $imageFolder = $configGet->get('imageFolder');
    }
    $this->imageFolder    = $imageFolder;

    // Debug settings
    $logFile = '';
    if(!null == $configGet->get('logFile') && !empty($configGet->get('logFile'))){
      $logFile = $configGet->get('logFile');
    }
    $this->logFile        = $logFile;
    $displayErrors = 0;
    if(!null == $configGet->get('displayErrors') && !empty($configGet->get('displayErrors'))){
      $displayErrors = $configGet->get('displayErrors');
    }
    $this->displayErrors  = $displayErrors;


    // Email settings
    $data['emailFrom']    = $this->emailFrom      = $configGet->get('emailFrom');
    $data['emailName']    = $this->emailName      = $configGet->get('emailName');
    $data['contactName']  = $this->contactName    = $configGet->get('contactName');

    $data['radarContact']  = $this->radarContact   = $configGet->get('radarContact');

    if($this->baseUrl !== ''){
      //using different settings for dev/release
    }

    // Define constants
    // Database
    defined('DB_HOST') ? null : define ('DB_HOST', $configGet->get('DB_HOST'));
    defined('DB_USER') ? null : define ('DB_USER', $configGet->get('DB_USER'));
    defined('DB_PASS') ? null : define ('DB_PASS', $configGet->get('DB_PASS'));
    defined('DB_NAME') ? null : define ('DB_NAME', $configGet->get('DB_NAME'));
    defined('DB_CHARSET') ? null : define ('DB_CHARSET', $configGet->get('CHARSET'));
    ini_set('default_charset', $configGet->get('CHARSET'));

    return $data;
  }
  /**
   * arrayLoader
   * @param  string $file      file path
   * @param  string $tableName name of the table
   * @return array
   */
  public function arrayLoader($file='', $item='')
  {
    if(empty($file) || empty($item)){
      return false;
    }
    $loader = new Loader();
    $content = $loader->loadContent($file, $item);
    //print_r($content->get('item'));
    if(!empty($content->get('item'))){
        return $content->get('item');
    }
    return false;
  }

}
