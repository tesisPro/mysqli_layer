<?php //File: library/traits/EncryptionTrait.php
/**
 * Summary: manipulating encrypted data or creating encrypted data
 */
/**
 * EncryptionTrait
 *
 * PHP version 5.6 /7.0
 *
 * @subpackage Custom_framework
 * @author     Tereza Simcic <tereza.simcic@gmail.com>
 * @copyright  2016 Tereza Simcic
 * @license    Tereza Simcic
 *
 *
 * @link       http://tesispro.com
 * @name       EncryptionTrait.php
 *
 */

namespace Tesis\Traits;

trait EncryptionTrait
{
  /**
   * base64_url_encode custom base64_encode without conflicting chars for url
   *
   * @param  string $data string to encode
   *
   * @return string
   */
  public function base64_url_encode($data)
  {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
  }
  /**
   * base64_url_decode custom base64_decode without conflicting chars for url
   *
   * @param  string $data base64_url_encoded string
   *
   * @return string
   */
  public function base64_url_decode($data)
  {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
  }
  /**
   * generateHash generate hash to authorize user/customer, needed for sending verifications
   *
   * @param $data compound data that will be hashed (SFN + date, date + username+ user agent)
   *
   * @return string
   *
  */
  public function generateHash($data='')
  {
    if(empty($data)){
      return false;
    }
    $hash = hash_hmac("sha256", $data, ENC_KEY);

    return $hash;
  }
  /**
   * passwordHashCost - benchmark your server to determine how high of a cost you can
   * afford. You want to set the highest cost that you can without slowing down
   * you server too much. 8-10 is a good baseline, and more is good if your servers
   * are fast enough. The code below aims for ≤ 50 milliseconds stretching time,
   * which is a good baseline for systems handling interactive logins.
   *
   * @return integer optimal cost for current server
   */
  private function passwordHashCost()
  {
    $timeTarget = 0.05; // 50 milliseconds
    $cost = 8;
    do {
      $cost++;
      $start = microtime(true);
      password_hash("test", PASSWORD_BCRYPT, ["cost" => $cost]);
      $end = microtime(true);
    } while (($end - $start) < $timeTarget);

    //echo "Appropriate Cost Found: " . $cost . "\n";
    return $cost;
  }
  /**
   * setPassword  set secure password using password_hash with cost option
   *              for this server optimal cost=10
   *              for login use password_needs_rehash - if returns true, rehash password and save it
   *
   * @param string $password plain text password to hash
   *
   * @return string
   *
  */
  public function setPassword($password) {
    $options = [
      'cost'=> 14 //or run passwordHashCost
    ];
    return password_hash($password, PASSWORD_DEFAULT, $options);
  }
  /**
   * verifyPassword verify if entered and saved passwords equals
   *
   * @param  string $password       entered password
   * @param  string $hashedPassword hashed password (saved in db)
   *
   * @return bool
   */
  public function verifyPassword($password, $hashedPassword)
  {
    $options = [
      'cost'=> 14 //or run passwordHashCost
    ];
    if (password_verify($password, $hashedPassword)) {
      // Success - Now see if their password needs rehashed
      if (password_needs_rehash($hashedPassword, PASSWORD_DEFAULT, $options)) {
        // We need to rehash the password, and save it.  Just call setPassword
        $this->setPassword($password);
        error_log('Password: to rehash and save in db');
      }
      return true; // Or do what you need to mark the user as logged in.
    }
    return false;
  }
  /**
   * generatePassword generate 10 character long password
   *
   * @return string
   */
  public function generatePassword()
  {
    $password = implode('', array_map(function () { return chr(rand(0, 1) ? rand(48, 57) : rand(97, 122)); }, range(0, 9)));
    return $password;
  }
}
