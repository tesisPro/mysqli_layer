<?php //File: library/system/MysqliLayer.php
/* vim: set expandtab tabstop=2 shiftwidth=2 softtabstop=2: */
/**
 * Summary: database mysqli layer
 */
/**
 * MysqliLayer - A class for manipulating database, using mysqli driver
 *
 * PHP version 5.6 / 7.0
 *
 * @subpackage Custom_framework
 * @author     Tereza Simcic <tereza.simcic@gmail.com>
 * @copyright  2016 Tereza Simcic
 * @license    Tereza Simcic
 *
 *
 * @link       http://tesispro.com
 * @name       MysqliLayer.php
 *
 * @usage:
 *
 */

namespace Tesis\System;

use Tesis\Faces\ExceptionInterface;
use Tesis\Traits\LoaderTrait;
use \mysqli as mysqli;

class MysqliLayer implements ExceptionInterface
{
  use LoaderTrait;

  /**
  * @access public
  * @var obj
  */
  public $db = null;
  /**
  * @access public
  * @var string
  */
  public $table;
  /**
  * @access public
  * @var array
  */
  public $tableFields = [];
  /**
  * @access public
  * @var string
  */
  public $tablePK;
  /**
  * @access public
  * @var string
  */
  public $tableAI;
  /**
  * @access public
  * @var array
  */
  public $tables = [];
  /**
  * @access public
  * error mesages we might want to return
  * @var string
  */
  public $error = '';
  /**
  * @access public
  * @var string
  */
  public $sql = '';

  /**
   * __construct establish connection if is null
   */
  public function __construct()
  {
    //DB_USER, DB_PASS, DB_NAME, DB_HOST
    $this->user = DB_USER;
    $this->password = DB_PASS;
    $this->database = DB_NAME;
    $this->host = DB_HOST;
    try{
      $this->db = $this->connect();
    }
    catch(Exception $e){
      $this->error = DB_CONN_NULL;
      $this->debug(__METHOD__, true);
      $this->db = null;
    }
    return false;
  }
  /**
   * connect connect to mysqli db
   *
   * @return object
   */
  protected function connect()
  {
    $mysqli = new mysqli($this->host, $this->user, $this->password, $this->database);
    if ($mysqli->connect_errno) {
var_dump($mysqli->connect_errno);
exit;
      throw new Exception('Connect Error: ' . $mysqli->connect_errno . ' ' . $mysqli->connect_error);
      $mysqli = null;
    }
    else{
      $mysqli->set_charset('utf8');
    }
    return $mysqli;
  }
  /**
   * query execute query and return object (using fetch_object MODE)
   *
   * @param  string  $query
   * @param  boolean $update if CRUD operation, set to true
   *
   * @return array of objects
   */
  //public function query($query, $update=false)
  public function query($query)
  {
    if(null === $this->db){
      $this->error = "\n" . DB_CONN_NULL . ": " . $query ."\n";
      $this->debug(__METHOD__, true);
      return false;
    }
    $this->sql = $query;
    $result = $this->db->query($query);

    if(!$result){
      //error_log('Query not executed missing tables' . $query ."\n");
      return false;
    }

    $results = [];
    if($result){
      while ( $row = $result->fetch_object() ) {
        $results[] = $row;
      }
    }

    return $results;

  }

  /**
   * executeBySql provided for executing special case queries
   *
   * @param string $query
   *
   * @return bool
   */
  public function executeBySql($query='')
  {
    if(null === $this->db){
      $this->error = "\n" . DB_CONN_NULL . ": " . $query ."\n";
      $this->debug(__METHOD__, true);
      return false;
    }
    if(empty($query)){
      $this->error = "\nMissing query \n";
      return false;
    }
    $this->sql = $query;
    $result = $this->db->query($query);
    $this->debug(__METHOD__);
    return $result ? $result : false;
  }
  /**
   * debug debug processing
   *
   * @param string $method
   * @param string $log if true, output will be written into error_log file
   *
   * @access protected
   * @return bool
   * @usage: echo $this->db->error
   */
  public function debug($method, $log=false)
  {
    $this->error = $method . "\n";
    $output = "\n Last query: ". $this->sql;
    if($this->db->error === ''){
      if($this->db->affected_rows == 0){
        $this->error .= ' not saved: ' . $this->db->affected_rows . $output;
      }
      else{
        $this->error .=  'saved: ' . $this->db->affected_rows;
      }
    }
    else{
      $this->error .= ' ERR not saved: ' . $this->db->error . ' ' . $this->db->errno. $output;
    }
    if($log === true){
      error_log($this->error);
    }
  }
  /**
   * getTables for insert and update get properties of the table
   *           for safer processing
   *
   * @param string $tableName
   *
   * @return bool
   */
  public function getTables($tableName)
  {
    $filename = LIB_PATH . 'mappers/dbTablesMapper.php';
    if(defined('TABLE_MAPPER')){
      $filename = TABLE_MAPPER;
    }
    $mapper = $this->arrayLoader($filename, $tableName);
    return $mapper;
  }
  /**
   * insertIntoTable
   *
   * @param array  $data array of data to process(insert, update)
   * @param string [$table] (class variable) table name
   * @param array  [$tableFields] (class variable) table fields
   *
   * @return bool
   */
  public function insertIntoTable($tableName='', array $data=null)
  {
    if(is_null($data) || empty($tableName)){
      $this->error =  __METHOD__ .' empty data';
      return false;
    }
    $tableProp = $this->getTables($tableName);
    //get table mapper
    if(!$tableProp){
      $this->error =  __METHOD__ .' table ' . $tableName .' is not mapped';
      return false;
    }
    $this->table = $tableName;
    $this->tableFields = $tableProp['fields'];
    $this->tablePK = array_shift($tableProp['PK']);
    $this->tableAI = array_shift($tableProp['AI']);
    if(empty($this->tableAI)){
      if(!array_key_exists($this->tablePK, $data) || empty($data[$this->tablePK])){
        $this->error = ' missing value for PK '. $this->tablePK;
        return false;
      }
    }
    //if autoincrement is empty, make sure to get value of next PK
    //build values and insert
    $fields = [];
    $values = [];
    foreach ($data as $key => $val) {
      // check if passed fields are in table fields array
      // if exists continue, otherwise skip
      // make sure values are trimmed
      if(in_array($key, $this->tableFields)){
        $fields[] = $key;
        if(is_string($val)){
          $values[] = "'" . trim($val) . "'";
        }
        else{
          $values[] = "'" . $val . "'";
        }
      }
    }
    if(empty($fields)){
      $this->error =  ' empty fields';
      return false;
    }
    $fields = join(',', $fields);
    $values = join(',', $values);
    // check quotation-> we have serialized data!!!
    $sql = " INSERT INTO " . $this->table ." (". $fields .") VALUES (".$values.") ";
    $insert = $this->executeBySql($sql);

    // Error handling
    $this->debug(__METHOD__);
    // returnId: If requested lastInsertID
    if(array_key_exists('returnId', $data) && $data['returnId'] == 1){
      $insert = $lastId = $this->db->insert_id;
      $this->error =  ' returned lastId: ' . $lastId . ' insert: ' . $insert;
    }
    return $insert ? $insert : false;
  }
  /**
   * updateTable
   *
   * @param array  $data array of data to process(insert, update)
   * @param string [$table] (class variable) table name
   * @param array  [$tableFields] (class variable) table fields
   * @param array  [$tablePK] (class variable) primary key of the table
   * @param array  [$tableAutoincrement] (class variable)
   *                                     if PK has attribute autoincrement
   *                                     skip PK
   * exclude PK from searching if fields exists, but
   * PK value should be known for updating the table
   * @return bool
   */
  public function updateTable($tableName, array $data=null)
  {
    if(is_null($data) || empty($tableName)){
      $this->error =  __METHOD__ .' empty data';
      return false;
    }
    $tableProp = $this->getTables($tableName);
    //get table mapper
    if(!$tableProp){
      $this->error =  __METHOD__ .' table ' . $tableName .' is not mapped';
      return false;
    }
    $this->table = $tableName;
    $this->tableFields = $tableProp['fields'];
    $this->tablePK = array_shift($tableProp['PK']);
    $this->tableAI = array_shift($tableProp['AI']);
    if(empty($this->tableAI)){
    }
    
    $update = [];
    foreach ($data as $key => $val) {
      //skip id field for autoincremented table - only
      if(in_array($key, $this->tableFields) && $key != $this->tablePK){
        if(is_string($val)){
          $update[] = $key . "='" . trim($val) . "'";
        }
        else{
          $update[] = $key . "='" . $val . "'";
        }
      }
    }
    if(empty($update)){
      return false;
    }
    $update = join(",", $update);
    $tablePKValue = $data[$this->tablePK];

    $sql = "UPDATE ".$this->table." set " . $update;
    $sql .= " WHERE ". $this->tablePK." = '" . $tablePKValue ."'";
    $update = $this->executeBySql($sql);

    // Error handling
    $this->debug(__METHOD__);
    return $update ? $update : false;
  }
  /**
   * deleteFromTable
   *
   * @param string $tableName
   * @param array  $data should contain 'name', ie field name and 'value' value for that name
   *
   * TODO: search for table details and use PK of the table to delete a record, ... ?
   *
   * @return bool
   */
  public function deleteFromTable($tableName='', array $data=null)
  {
    if(is_null($data) || empty($tableName)){
      return false;
    }
    //data should contain 'where' attributes: name of the field(id) and value
    $sql = "DELETE FROM " . $tableName . " WHERE ". $data['name']. " = " . $data['value'];
    //using mysqli query, not model
    return $this->executeBySql($sql);
  }
  /**
   * selectRows select all rows with defined fields
   *
   * @param string $tableName
   * @param string $field, default '*'
   *
   * @return string
   */
  public function selectRows($tableName, $fields='*')
  {
    if(empty($tableName)){
      return false;
    }
    $sql = "SELECT " . $fields . " FROM " . $tableName;
    return $this->selectBySql($sql);
  }
  /**
   * selectRow select specific row with defined fields, returns only one row (object)
   *
   * @param string $tableName
   * @param string $field, default '*'
   *
   * @return object/bool
   */
  public function selectRow($tableName, $fields='*')
  {
    if(empty($tableName)){
      return false;
    }
    $sql  = " SELECT " . $fields . " FROM " . $tableName;
    $sql .= " LIMIT 1";
    $res = $this->selectBySql($sql);

    return $res ? reset($res) : $res;
  }
  /**
   * selectByField select field
   *
   * @param string $tableName
   * @param string $fieldName
   * @param string $fieldValue
   *
   * @return array
   */
  public function selectByField($tableName='', $fieldName='', $fieldValue='')
  {
    if(empty($tableName) || empty($fieldName) || empty($fieldValue)){
      return false;
    }

    $tableProp = $this->getTables($tableName);
    if(!$tableProp){
      $this->error = __METHOD__ .' table ' . $tableName .' is not mapped';
      return false;
    }
    if(!in_array($fieldName, $tableProp['fields'])){
      $this->error = __METHOD__ . ' fieldName not in array';
      return false;
    }
    $sql  = " SELECT * FROM " . $tableName ;
    $sql .= " WHERE ".$fieldName."='" .$fieldValue;
    $sql .= "' LIMIT 1";
    return $this->selectBySql($sql);
  }
  /**
   * selectBySql select by sql
   *
   * @param string $sql
   *
   * @return array
   */
  public function selectBySql($sql='')
  {
    if(empty($sql)){
      return false;
    }
    $results = $this->query($sql);
    return $results ? $results : false;
  }

/*------------ end of class -------------------*/
}
