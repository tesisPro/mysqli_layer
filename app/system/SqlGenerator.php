<?php
/* vim: set expandtab tabstop=2 shiftwidth=2 softtabstop=2: */
/**
 * Summary: SqlGenerator class provides mapping of all tables: fields,PK,AI
 */
/**
 * BaseModel class- class for databases
 *                   (dealing with select/insert/update/delete sql statements)
 *
 * PHP version 5.6 / 7.0
 *
 * @package    myProject
 * @subpackage Custom_framework
 * @author     Tereza Simcic <tereza.simcic@gmail.com>
 * @copyright  2016 Tereza Simcic
 * @license    Tereza Simcic
 *
 *
 * @link       http://tesispro.com
 * @name       sqlGenerator.php
 * @usage: sqlGenerator should be called as needed:
 * - if tableMapping.php file does not exist
 * - on each change in DB schema like: inserting fields, changing
 * primary key or auto_increment
 * - it is added at the top of BaseModel - as we need it here
 */

namespace Tesis;


class SqlGenerator
{
  /**
  * @access public
  * @var object
  */
  public $conn;
  /**
  * @access public
  * @var string
  */
  public $database;
  /**
  * @access public
  * @var array
  */
  public $tables = [];
  /**
  * @access public
  * needed only for output
  * @var array
  */
  public $tableNames = [];
  /**
  * @access public
  * @var string
  */
  public $path;


  const DIR_ADDR = 'mappers/dbTablesMapper.php';

  /**
   * __construct
   *
   * @param object $conn
   *
   * @return
   */
  public function __construct( $conn=null)
  {
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    $this->conn = $conn;
    $this->date = date('m/d/Y H:i:s');
    $this->database = DB_NAME;
  }
  public function getAllTables()
  {
    $sql = "SHOW TABLES FROM " . $this->database .";";
    $res = $this->conn->query($sql);

    if(!$res){
      return false;
    }
    $tables = [];
    while($row = $res->fetch_row()){
      $row = reset($row);
      $tables[] = $row;
    }
    $this->tableNames = $tables;
    $content = [];
    foreach($tables as $table){
      $content[] = $this->getAllFields($table);
    }

    if(!empty($content)){
      $content = join('', $content);
      $this->tables = $content;
      $this->generatePHPFile($content);
    }
  }

  public function getAllFields($tableName='')
  {
    if(empty($tableName)){
      return false;
    }

    $sql = "DESCRIBE $tableName";
    $result = $this->conn->query($sql);
    if(!$result){
      continue;
    }
    $rows = [];
    while($row = $result->fetch_array()){
      $rows[] = $row;
    }

    if(is_array($rows) && !empty($rows)){
      $parsed = $this->parseRows($tableName, $rows);
      return $parsed;
    }
  }
  public function parseRows($tableName='', array $rows=null)
  {
    if(is_null($rows) || empty($tableName)){
      return false;
    }
    $keyField = "Field";
    $keyType = "Type";
    $tablePK = "";
    $tableAI = "";
    $fields = [];
    foreach($rows as $key => $record){
      if($key == 'Extra' && !empty($record['Extra'])){
        $tableAI = $record[$keyField];
      }
      if($key == 'PRI' && !empty($record)){
        $tablePK = $record[$keyField];
      }
      $tableFields[] = $record[$keyField];
    }

    $f = "'".implode("','", $tableFields)."'";
    $f = "[" . $f . "]";

    $arr = [];
    $arr[] =  "'fields' => ". $f ."";
    $arr[] =  "'PK' => ['". $tablePK ."']";
    $arr[] =  "'AI' => ['". $tableAI ."']";

    $arr = join(',', $arr);

    $arr = "'". $tableName  ."' => " . "[". $arr ."],";

    $arr .= "\n\n";
    return $arr;
  }

  public function generatePHPFile($allContent='')
  {
    if(empty($allContent)){
      return false;
    }
    // Write the content to the file
    $content = '';
    $content .= "<?php ";
    $content .= "//File: ". self::DIR_ADDR;
    $content .= "\n\t\t\t//Generated: ".$this->date."\n\n";
    //don't miss 'return ' ->otherwise arrays will not be searched, error in loader
    $content .= "return $". "tables = [\n" ;
    $content .= "\t\t" . $allContent;
    $content .= "\n];\n";
    $content .= "\n\n";

    file_put_contents(LIB_PATH.self::DIR_ADDR, $content);
  }
  public function readTables()
  {
    return $this->tables;
  }
}
//it can be run only by cli
if ( php_sapi_name() === 'cli' ) {
  $tableMapper = LIB_PATH.'mappers/dbTablesMapper.php';
  $gen = new SqlGenerator();
  $tables = $gen->getAllTables();
  //read new date
  if(file_exists($tableMapper)){
    $line = file($tableMapper)[1];
    echo $line = substr(trim($line),'2') . PHP_EOL;
    print_r($gen->tableNames);
  }
}
