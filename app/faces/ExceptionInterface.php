<?php //File: library/faces/ExceptionInterface.php
/* vim: set expandtab tabstop=2 shiftwidth=2 softtabstop=2: */
/**
 * Summary: managing exceptions
 */
/**
 * ExceptionInterface - interface for managing exceptions
 *
 * PHP version 5.6
 *
 * @subpackage Custom_framework
 * @author     Tereza Simcic <tereza.simcic@gmail.com>
 * @copyright  2015 Tereza Simcic
 * @license    Tereza Simcic
 *
 *
 * @link       http://tesispro.com
 * @name       ExceptionInterface.php
 *
 *
 */

namespace Tesis\Faces;

interface ExceptionInterface
{
  const MISSING_ARGUMENTS  = 'missing arguments';
  const MISSING_DIR        = 'missing directory';
  const EMPTY_VALUES       = 'empty values';
  const NO_RECORDS         = 'no records';
  const NO_PHOTOS          = 'no photos';
  const ALL_NULL           = 'all null';
  const NO_INTERVALS       = 'no intervals';
  const NO_LANES           = 'no lanes';
  const WRONG_VALUE        = 'wrong value';
  const PERMISSION_DENIED  = 'permission denied';
  const NO_CONFIG          = 'no config';

  const DEFAULT_GEO_COORD  = '29.63370412589063,-95.57670146226883';
  const LOCATION_ROUTE     = 'location/';
  const DEFAULT_IMAGE      = 'default75.png';
  const IMAGE_PATH         = 'assets/images/';
  const DB_CONN_NULL       = 'Query not executed - connection is null';
}
